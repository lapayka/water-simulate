\babel@toc {russian}{}\relax 
\contentsline {chapter}{Введение}{4}{chapter*.2}%
\contentsline {chapter}{\numberline {1}Аналитическая часть}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}Постановка задачи}{5}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Входные и выходные данные}{5}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Ограничения}{5}{subsection.1.1.2}%
\contentsline {section}{\numberline {1.2}Математическая модель}{5}{section.1.2}%
\contentsline {section}{\numberline {1.3}Анализ методов для генерации водной поверхности}{5}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Решение уравнений Навье-Стокса}{6}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Метод Лагранжа}{6}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Спектральный метод}{6}{subsection.1.3.3}%
\contentsline {section}{\numberline {1.4}Построение трехмерного изображения}{7}{section.1.4}%
\contentsline {section}{\numberline {1.5}Алгоритмы удаления невидимых линий}{7}{section.1.5}%
\contentsline {subsection}{\numberline {1.5.1}Алгоритм Робертса}{7}{subsection.1.5.1}%
\contentsline {subsection}{\numberline {1.5.2}Алгоритм, использующий трассировку лучей}{7}{subsection.1.5.2}%
\contentsline {subsection}{\numberline {1.5.3}Алгоритм Варнока}{8}{subsection.1.5.3}%
\contentsline {subsection}{\numberline {1.5.4}Алгоритм, использующий Z-буфер}{8}{subsection.1.5.4}%
\contentsline {subsection}{\numberline {1.5.5}Закраска Фонга}{8}{subsection.1.5.5}%
\contentsline {subsection}{\numberline {1.5.6}Закраска Гуро}{9}{subsection.1.5.6}%
\contentsline {section}{\numberline {1.6}Модель освещения}{9}{section.1.6}%
\contentsline {subsection}{\numberline {1.6.1}Локальная модель освещения}{9}{subsection.1.6.1}%
\contentsline {subsection}{\numberline {1.6.2}Глобальная модель освещенности}{9}{subsection.1.6.2}%
\contentsline {section}{\numberline {1.7}Обзор решений для взаимодействия с GPU}{10}{section.1.7}%
\contentsline {section}{\numberline {1.8}Вывод из аналитической части}{10}{section.1.8}%
\contentsline {chapter}{\numberline {2}Конструкторская часть}{11}{chapter.2}%
\contentsline {section}{\numberline {2.1}Спектральный метод}{11}{section.2.1}%
\contentsline {section}{\numberline {2.2}Трингуляция}{14}{section.2.2}%
\contentsline {section}{\numberline {2.3}Растеризация}{17}{section.2.3}%
\contentsline {section}{\numberline {2.4}Структуры данных}{19}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}Структура данных для карты высот}{19}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Структура данных для объектов сцены}{19}{subsection.2.4.2}%
\contentsline {section}{\numberline {2.5}Вывод из конструкторской части}{19}{section.2.5}%
\contentsline {chapter}{\numberline {3}Технологическая часть}{20}{chapter.3}%
\contentsline {section}{\numberline {3.1}Средства реализации}{20}{section.3.1}%
\contentsline {section}{\numberline {3.2}Структура программы}{20}{section.3.2}%
\contentsline {section}{\numberline {3.3}Реализация алгоритмов}{21}{section.3.3}%
\contentsline {section}{\numberline {3.4}Интерфейс программы}{28}{section.3.4}%
\contentsline {section}{\numberline {3.5}Тестирование}{28}{section.3.5}%
\contentsline {section}{\numberline {3.6}Вывод из технологической части}{31}{section.3.6}%
\contentsline {chapter}{\numberline {4}Исследовательская часть}{32}{chapter.4}%
\contentsline {section}{\numberline {4.1}Технические характеристики ЭВМ}{32}{section.4.1}%
\contentsline {section}{\numberline {4.2}Анализ скорости выполнения реализаций алгоритмов}{32}{section.4.2}%
\contentsline {section}{\numberline {4.3}Вывод из экспериментальной части}{34}{section.4.3}%
\contentsline {chapter}{Заключение}{35}{chapter*.19}%
\contentsline {chapter}{Список литературы}{36}{chapter*.20}%
