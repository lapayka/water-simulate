import matplotlib.pyplot as plt

file = open("times.txt", "r")

n = list(map(float, file.readline().split()))
CPU = list(map(float, file.readline().split()))
GPU = list(map(float, file.readline().split()))

plt.plot(n, CPU, label = "CPU")
plt.plot(n, GPU, label = "GPU")
plt.xlabel("Линейный размер сетки")
plt.ylabel("Время(мс)")
plt.legend()

plt.show()
    
file.close()


