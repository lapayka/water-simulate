#pragma once

struct gPoint;

void ggenerateSpectr(void* h0, int n, int m, double Lx, double Lz, double velocity);

void greCountAmplitudes(void* h0, void* h01, int n, int m, double Lx, double Lz, double t);

void ggenerateHeightMap(void* h, void* h0, int n, int m, double Lx, double Lz);

void* cMalloc(int size);

void cFree(void* ptr);

void CopyFromDevToHost(void* dst, void* src, int size);

void CopyFromHostToDev(void* dst, void* src, int size);

void devSync();

double ElTime(void* h, void* h0, int n, int m, double Lx, double Lz, double t);

void gCreategPoligons(void* pols, int n, int m, void* h, double Lx, double Lz, double angle, gPoint light);