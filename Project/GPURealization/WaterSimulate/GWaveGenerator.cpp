#include "GWaveGenerator.h"


#define A 0.5
# define RAND_COUNT 12
#define g 9.8
#define EPS 1e-6

static inline double generateNormal()
{
	double r = 0.0;
	for (int i = 0; i < RAND_COUNT; i++)
		r += (2.0 * rand()) / RAND_MAX;

	return r / RAND_COUNT - 1;
}

static inline double Ph(const Point &k, double velocity)
{
	double k_2 = k.sqr();
	double L = velocity * velocity / g;


	return A / (k_2 * k_2) * k.y * k.y * exp(-1.0 / (k_2 * L * L));
}

static inline complex<double> h(const Point &k, double velocity)
{
	double Ei = generateNormal(), Ej = generateNormal();

	return (0.25 / (sqrt(2.0))) * complex(Ei, Ej) * sqrt(Ph(k, velocity));
}

static inline void generateSpectr(const shared_ptr<HeightMap> &hMap, double velocity)
{
	Matrix<complex<double>> &mat = *(hMap->h0.get());
	double Lx = hMap->Lx;
	double Lz = hMap->Lz;

	int n_2 = mat.n() / 2;
	int m_2 = mat.m() / 2;

	for (int i = -n_2; i < n_2; i++)
		for (int j = -m_2; j < m_2; j++)
		{
			Point k(2.0 * 3.14 * i / Lx, 2.0 * 3.14 * j / Lz);
			complex<double> tmp = h(k, velocity);
			mat(i + n_2, j + m_2) = tmp;
		}
}

void GWaveGenerator::generateMaps(const shared_ptr<HeightMap>& hMap, const shared_ptr<NormalMap>& nMap, double time, double velocity)
{
	if (hMap->first_time)
	{
		generateSpectr(hMap, velocity);

		CopyFromHostToDev((void*)hMap->gh01, (void*)hMap->h01.get()->getArray(), hMap->n * hMap->m * sizeof(complex<double>));

		hMap->first_time = false;
	}
	greCountAmplitudes((void*)hMap->gh0, (void*)hMap->gh01, hMap->n, hMap->m, hMap->Lx, hMap->Lz, time);

	ggenerateHeightMap((void*)hMap->gh, (void*)hMap->gh0, hMap->n, hMap->m, hMap->Lx, hMap->Lz);
}
