#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include "../Matrix/matrix.h"
#include <memory>
#include <complex.h>
#include "waves.h"

using namespace std;


struct HeightMap
{
    int n, m;
    double Lx, Lz;
    bool first_time;


    shared_ptr<Matrix<complex<double>>> h0;
    shared_ptr<Matrix<complex<double>>> h01;
    shared_ptr<Matrix<complex<double>>> h;

    complex<double> * gh0;
    complex<double> * gh01;
    complex<double> * gh;

    HeightMap() = default;
    HeightMap(int _n, int _m, double _Lx, double _Lz) :
        n(_n),
        m(_m),
        Lx(_Lx),
        Lz(_Lz),
        first_time(true),
        h0(new Matrix<complex<double>>(n, m)),
        h01(new Matrix<complex<double>>(n, m)),
        h(new Matrix<complex<double>>(n, m))
    {
        gh0 = (complex<double> *)cMalloc(n * m * sizeof(n*m));
        gh01 = (complex<double> *)cMalloc(n * m * sizeof(n * m));
        gh0 = (complex<double> *)cMalloc(n * m * sizeof(n * m));
    }

    ~HeightMap()
    {
        cFree((void*)gh0);
        cFree((void*)gh01);
        cFree((void*)gh);
    }
};

#endif // HEIGHTMAP_H
