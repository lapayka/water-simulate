#ifndef BASEWAVEGENERATOR_H
#define BASEWAVEGENERATOR_H

#include <memory>
#include "../Maps/heightmap.h"
#include "../Maps/normalmap.h"

using namespace std;

class BaseWaveGenerator
{
protected:
    double a;
public:
    BaseWaveGenerator() = default;
    virtual void generateMaps(const shared_ptr<HeightMap> &hMap, const shared_ptr<NormalMap> &nMap, double time, double velocity) = 0;
};

#endif // BASEWAVEGENERATOR_H
