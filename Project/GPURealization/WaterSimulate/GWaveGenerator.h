#pragma once
#include "WaveGenerator/basewavegenerator.h"

#include "waves.h"
#include "Poligons/point.h"

class GWaveGenerator : public BaseWaveGenerator
{
public:
	GWaveGenerator() = default;
	virtual void generateMaps(const shared_ptr<HeightMap> &hMap, const shared_ptr<NormalMap> &nMap, double time, double velocity) override;
};

