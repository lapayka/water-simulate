#include "craytracer.h"

#define K 0.6

Color CRayTracer::trace(const Poligon *pols, int n, const Ray &ray, const Point &light, double ymax, double ymin)
{
    for (int i = n - 1; i >= 0; i--)
    {
        Point inter_point;
        if (ray.intersect(pols[i], inter_point))
        {
            Point reflected(Ray(Point(0,0,0), light).reflect(pols[i]));

            double I = fabs(K * (pols[i].normal * (light)) + (1-K) * (reflected * (-ray.vector)));
            return Color(0,149,182) * I;
        }
    }
    return Color(0,0,0);
}
void CRayTracer::draw(Color *colors, int n, int m, Poligon *pols, int pol_size, const Point &light, double ymax, double ymin)
{
    for (int i = (ymin < 0 ? 0 : ymin); i < (ymax > n ? n : ymax); i++)
        for (int j = 0; j < m; j++)
        {
            (colors[i * m + j] = trace(pols, pol_size, Ray(Point(j, i, -10000.0), Point(0.0,0.0,1.0)), light, ymax, ymin));
        }
}
