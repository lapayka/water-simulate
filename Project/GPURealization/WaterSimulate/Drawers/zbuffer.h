#ifndef ZBUFFER_H
#define ZBUFFER_H

#include "basert.h"

struct ZElem
{
    ZElem() = default;
    Color color;
    double z;
};

class ZBuffer : public BaseRT
{
public:
    ZBuffer() = default;

    virtual void draw(Color *colors, int n, int m, Poligon *pols, int pol_size, const Point &light, double ymax, double ymin) override;
};

#endif // ZBUFFER_H
