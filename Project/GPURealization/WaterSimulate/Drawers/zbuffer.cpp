#include "zbuffer.h"

#include "float.h"



void drawLine(double *z, Color *colors, int curx1, int curx2, int cury, double curz1, double curz2, double curIab, double curIac)
{
    if (curx2 < curx1)
    {
        swap(curx1, curx2);
        swap(curz2, curz1);
        swap(curIab, curIac);
    }

    double dI = (curIac - curIab) / (curx2 - curx1);
    double dz = (curz2 - curz1) / (curx2 - curx1);
    for (int i = curx1; i <= curx2; i++)
    {
        int index = i + cury;
        if (index > 0 && index < 800000)
            if (curz1 < z[index])
            {
                colors[index] =  Color(0,149,182) * curIab;
                z[index] = curz1;
            }
        curIab += dI;
        curz1 += dz;
    }
}

void drawTriangle(double *z, Color *colors, const Point &A, const Point &B, const Point &C, double IA, double IB, double IC, int m)
{
    double xa = A.x;
    int    ya = int(A.y);
    double za = A.z;

    double xb = B.x;
    int    yb = int(B.y);
    double zb = B.z;

    double xc = C.x;
    int    yc = int(C.y);
    double zc = C.z;

    if (ya < yb)
    {
        swap(xa, xb);
        swap(ya, yb);
        swap(za, zb);
    }
    if (ya < yc)
    {
        swap(xa, xc);
        swap(ya, yc);
        swap(za, zc);
    }
    if (yb < yc)
    {
        swap(xb, xc);
        swap(yb, yc);
        swap(zb, zc);
    }

    int dyab = (ya - yb);
    double dzab = (zb - za) / dyab;
    double dxab = double(xb - xa) / dyab;
    double dIab = (IB - IA) / dyab;

    int dyac = (ya - yc);
    double dzac = (zc - za) / dyac;
    double dxac = double(xc - xa) / dyac;
    double dIac = (IC - IA) / dyac;

    int len = dyab;

    double curIab = IA, curIac = IA;
    double curx1 = xa;
    double curx2 = xa;
    int cury = ya;
    double curz1 = za;
    double curz2 = za;

    for (int i = 0; i < len; i++)
    {
        drawLine(z, colors, curx1, curx2, cury * m, curz1, curz2, curIab, curIac);
        curx1 += dxab;
        curx2 += dxac;

        cury -= 1;

        curz1 += dzab;
        curz2 += dzac;

        curIab += dIab;
        curIac += dIac;
    }

    int dybc = (yb - yc);
    len = dybc;
    double dzbc = (zc - zb) / dybc;
    double dxbc = double(xc - xb) / dybc;
    double dIbc = (IC - IB) / dybc;
    curx1 = xb;
    for (int i = 0; i <= len; i++)
    {
        drawLine(z, colors, curx1, curx2, cury * m, curz1, curz2, curIab, curIac);
        curx1 += dxbc;
        curx2 += dxac;

        cury -= 1;
        curz1 += dzbc;
        curz2 += dzac;

        curIab += dIbc;
        curIac += dIac;
    }

}

void ZBuffer::draw(Color *colors, int n, int m, Poligon *pols, int pol_size, const Point &light, double ymax, double ymin)
{
    int count = n * m;
    double *z = new double[(unsigned long)(count)];
    for (int i = 0; i < count; i++)
        z[i] = DBL_MAX;

    for (int i = pol_size - 1; i >= 0; i -= 1)
    {
        drawTriangle(z, colors, pols[i].A, pols[i].B, pols[i].C, pols[i].Ia, pols[i].Ib, pols[i].Ic, m);
    }

    delete[] z;
}
