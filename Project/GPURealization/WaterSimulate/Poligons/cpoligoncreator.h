#ifndef CPOLIGONCREATOR_H
#define CPOLIGONCREATOR_H

#include "poligoncreator.h"
#include "../DrawManager/basedrawmanager.h"

class CPoligonCreator : public PoligonCreator
{
public:
    CPoligonCreator() = default;
    virtual ~CPoligonCreator() = default;

    virtual shared_ptr<Poligons> getPoligons(const shared_ptr<HeightMap> &hMap, const Point &light,  double angle) override;
};

#endif // CPOLIGONCREATOR_H
