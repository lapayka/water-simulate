#include "cpoligoncreator.h"

#include "float.h"
#include <stdarg.h>

#define EPS 1e-6

#define K 0.8

int sign(int c)
{
    if (c < 0)
        return -1;
    else if (c > 0)
        return 1;
    else
        c = 0;

    return c;
}


Point reflect(const Point &vec, const Point &normal)
{
    return vec - normal * 2.0 * (vec * normal);
}

double intensity(const Point &light, const Point &normal)
{
    Point reflected(reflect(light, normal));
    double Iref = (reflected * (Point(0,0,-1)));
    Iref *= (1-K);

    double In = K * (normal * light) + (Iref > 0.0 ? Iref : 0.0) + 0.05;

    if (In < 0.5)
        cout << light << " " << normal << "\n";

    return (In < 1.0 ? In : 1.0);
}


void rotate_scale(Point &point, double angle, double k)
{
    double y = point.y;
    point.y = point.y * cos(angle) - point.z * sin(angle);
    point.z = (y * sin(angle) - point.z * cos(angle)) * k;
    point.x = point.x * k;
    point.y = -point.y * k + BaseDrawManager::WINDOWHEIGHT / 2;
}

void fillPointMat(Matrix<Point> &pmat, const shared_ptr<HeightMap> &hMap, double angle, int windWidth)
{
    Matrix<complex<double>> &mat = *(hMap->h.get());
    double x_step = hMap->Lx / mat.n();
    double z_step = hMap->Lz / mat.m();

    double k = BaseDrawManager::WINDOW_WIDTH / (hMap->Lx - x_step);
    double cur_z = -hMap->Lx / 2;



    for (int i = 0; i < mat.n(); i++)
    {
        double cur_x = 0;
        for (int j = 0; j < mat.m(); j++)
        {
            Point a(cur_x, mat(i, j).real(), cur_z);
            rotate_scale(a, angle, k);
            pmat(i, j) = a;
            cur_x += x_step;
        }
        cur_z += z_step;
    }
}

Point countNormal(const Point &a, const Point &b, const Point &c, const Point &light)
{
    Point n((b - a).vector_prod(c - a));
    if ((n * light) < 0)
        n = -n;
    return n;
}

void normalize(Point &normal)
{
    normal = normal * (1.0 / double(normal));
}

void fillNormalMat(Matrix<Point> &nmat, Matrix<Point> &pmat, const Point &light)
{
    memset(nmat.getArray(), 0, nmat.n() * nmat.m() * sizeof(Point));

    for (int i = 0; i < pmat.n() - 1; i++)
        for (int j = 0; j < pmat.m() - 1; j++)
        {
            Point n1(countNormal(pmat(i, j), pmat(i, j+1), pmat(i+1, j+1), light));
            Point n2(countNormal(pmat(i, j), pmat(i+1, j), pmat(i+1, j+1), light));

            //cout << n1 * n2 << " " << nmat(i +1, j + 1) * (n1 + n2) << " " << nmat(i, j) * (n1 + n2) << " " <<nmat(i, j+1) * n1 << " " << nmat(i+1, j) * n2 << "\n";

            nmat(i + 1, j + 1) += n1 + n2;
            nmat(i, j) += n1 + n2;
            nmat(i, j + 1) += n1;
            nmat(i + 1, j) += n2;
        }

    for (int i = 0; i < nmat.n(); i++)
        for (int j = 0; j < nmat.m(); j++)
            normalize(nmat(i, j));
}

void triangulate(Poligon *arr, Matrix<Point> &pmat, Matrix<Point> &nmat, const Point &light)
{
    for (int i = 0; i < pmat.n() - 1; i++)
    {
        for (int j = 0; j < pmat.m() - 1; j++)
        {
            *arr++ = Poligon(pmat(i, j), pmat(i + 1, j), pmat(i + 1, j + 1),
                             intensity(light, nmat(i, j)), intensity(light, nmat(i + 1, j)), nmat(i + 1, j + 1));
            *arr++ = Poligon(pmat(i, j), pmat(i, j + 1), pmat(i + 1, j + 1),
                             intensity(light, nmat(i, j)), intensity(light, nmat(i, j + 1)), nmat(i + 1, j + 1));
        }
    }
}

shared_ptr<Poligons> CPoligonCreator::getPoligons(const shared_ptr<HeightMap> &hMap, const Point &_light,  double angle)
{
    Matrix<complex<double>> &mat = *(hMap->h.get());
    shared_ptr<Poligons> pol(new Poligons((mat.n() - 1) * (mat.m() - 1) * 2));

    Point light = _light;
    rotate_scale(light, angle, 1.0);
    light.y = BaseDrawManager::WINDOWHEIGHT / 2 -  light.y;
    pol->setLight(light);

    //setbuf(stdout, NULL);

    Poligon *arr = pol->array().get();

    Matrix<Point> pMat(mat.n(), mat.m());
    Matrix<Point> nMat(mat.n(), mat.m());

    fillPointMat(pMat, hMap, angle, BaseDrawManager::WINDOW_WIDTH);
    fillNormalMat(nMat, pMat, light);

    triangulate(arr, pMat, nMat, light);




    //for (int i = 0; i < mat.m() - 1; i++)
    //{
    //    double cur_x = 0;
    //    for (int j = 0; j < mat.n() - 1; j++)
    //    {
    //        Point a(cur_x, mat(j,i).real(), cur_z);
    //        Point b(cur_x + x_step, mat(j+1, i).real(), cur_z);
    //        Point c(cur_x + x_step, mat(j + 1, i + 1).real(), cur_z + z_step);
    //        Point d(cur_x, mat(j, i + 1).real(), cur_z + z_step);
    //
    //        rotate_scale(a, angle, k);
    //        rotate_scale(b, angle, k);
    //        rotate_scale(c, angle, k);
    //        rotate_scale(d, angle, k);
    //
    //        y_min = min(5, a.y, b.y, c.y, c.y, y_min);
    //        y_max = max(5, a.y, b.y, c.y, c.y, y_max);
    //
    //        Point n1((c - a).vector_prod(d - a));
    //        n1 = n1 * (1.0 / (double(n1)));
    //        if ((n1 * light) < 0)
    //            n1 = -n1;
    //
    //
    //        Point n2((b - a).vector_prod(c - a));
    //        n2 = n2 * (1.0 / (double(n2)));
    //        if ((n2 * light) < 0)
    //            n2 = -n2;
    //
    //        //cout << a << " " << d << " " << c << "\n";
    //        //cout << a << " " << b << " " << c << "\n";
    //
    //        *arr++ = Poligon(a, d, c, n1, -n1 * a);
    //        *arr++ = Poligon(a, b, c, n2, -n2 * a);
    //        cur_x += x_step;
    //
    //    }
    //
    //    cur_z += z_step;
    //}



    return pol;
}

