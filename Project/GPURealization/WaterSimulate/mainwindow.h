#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Commands/facade.h"
#include "Commands/drawcommand.h"
#include "Commands/setparams.h"
#include "Commands/setvelocity.h"
#include "Commands/setgenerator.h"
#include "WaveGenerator/cwavegenerator.h"
#include "DrawManager/carcasmanager.h"
#include "AbstractFactory/abstractfactory.h"
#include <thread>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    QGraphicsScene *scene;

   bool generateMBOX(bool ind);

private slots:
    void on_pushButton_clicked();

    void on_V_E_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_radioButton_clicked();

    void on_radioButton_2_clicked();

    void on_pushButton_5_clicked();

private:
    bool isVelocitySet = false;
    bool isParamsSet = false;
    bool isExTimeSet = false;

    shared_ptr<BaseDrawManager> curManager;
    shared_ptr<AbstractFactory> factory;
    double angle = 0;
    double time = 0;
    int timeToExecute = 0;
    std::thread *work = nullptr;
    Ui::MainWindow *ui;
    shared_ptr<Scene> gscene;
};
#endif // MAINWINDOW_H
