#ifndef BASEDRAWMANAGER_H
#define BASEDRAWMANAGER_H

#include "waves.h"

#include <memory>
#include "../Scene/scene.h"
#include <QGraphicsScene>
#include <QTime>
#include <QCoreApplication>

using namespace std;

void sleepFeature();

class BaseDrawManager
{
protected:
    QGraphicsScene *scene;
public:
    enum { WINDOWHEIGHT = 800, WINDOW_WIDTH = 1000 };
    virtual void draw(Scene &gscene, double time, double angle, int count) = 0;
};

#endif // BASEDRAWMANAGER_H
