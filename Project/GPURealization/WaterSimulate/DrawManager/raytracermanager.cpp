#include "raytracermanager.h"

#define EPS 1e-6

#include <QTime>
#include <QCoreApplication>

void RayTracerManager::draw(Scene &gscene, double time, double angle, int count)
{
    Color **colors = (Color **)malloc(sizeof(Color *) * count);

    for (int i = 0; i < count; i++)
    {
        colors[i] = (Color*)calloc(BaseDrawManager::WINDOWHEIGHT * BaseDrawManager::WINDOW_WIDTH, sizeof(Color));
    }

    for (int i = 0; i < count; i++)
    {
        shared_ptr<Poligons> poligons = gscene.retScene(time, angle);
        time += 1.0 / 24;

        Poligon* pols = poligons->array().get();
        int n = poligons->size();

        traycer->draw(colors[i], BaseDrawManager::WINDOWHEIGHT, BaseDrawManager::WINDOW_WIDTH, pols, n, poligons->getLight(), poligons->getMax().y, poligons->getMin().y);
    }

    for (int i = 0; i < count; i++)
    {
        QImage im((uchar *)colors[i], BaseDrawManager::WINDOW_WIDTH, BaseDrawManager::WINDOWHEIGHT, QImage::Format_RGB888);
        scene->clear();
        scene->addPixmap(QPixmap::fromImage(im));
        scene->update();

        sleepFeature();
    }

    for (int i = 0; i < count; i++)
    {
         free(colors[i]);
    }

    free(colors);

}
