#include "carcasmanager.h"


void CarcasManager::drawPoligon(Poligon &pol, double k, double angle)
{
    drawLine(pol.A, pol.B);
    drawLine(pol.A, pol.C);
    drawLine(pol.B, pol.C);
}
void CarcasManager::draw(Scene &gscene, double time, double angle, int count)
{
    shared_ptr<Poligons> poligons = gscene.retScene(time, angle);

    double max_x = poligons->getMax().x;
    double min_x = poligons->getMin().x;

    double k = WINDOW_WIDTH / (max_x - min_x);

    shared_ptr<Poligon[]> pols = poligons->array();
    int n = poligons->size();

    scene->clear();
    for (int i = 0; i < n; i++)
    {
        drawPoligon(pols[i], k, angle);
        //sleepFeature();
    }

    scene->update();
}
