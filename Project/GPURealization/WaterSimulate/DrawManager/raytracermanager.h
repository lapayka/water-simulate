#ifndef RAYTRACERMANAGER_H
#define RAYTRACERMANAGER_H

#include "basedrawmanager.h"
#include "Drawers/basert.h"



class RayTracerManager : public BaseDrawManager
{
private:
    shared_ptr<BaseRT> traycer;

public:
    RayTracerManager(QGraphicsScene *_scene, const shared_ptr<BaseRT> &_traycer) {scene = _scene; traycer = _traycer;}
    virtual void draw(Scene &gscene, double time, double angle, int count) override;
};

#endif // RAYTRACERMANAGER_H
