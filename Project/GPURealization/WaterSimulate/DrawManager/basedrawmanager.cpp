#include "basedrawmanager.h"


void sleepFeature()
{
    QTime end = QTime::currentTime().addMSecs(40);
    while (QTime::currentTime() < end)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 1);
    }
    return;
}
