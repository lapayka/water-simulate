#ifndef LIGHTSOURCE_H
#define LIGHTSOURCE_H

#include "Poligons/point.h"

class LightSource
{
public:
    Point vector;

    LightSource(const Point &point) {vector = point; }
};

#endif // LIGHTSOURCE_H
