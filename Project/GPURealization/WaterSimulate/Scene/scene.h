#ifndef SCENE_H
#define SCENE_H

#include "camera.h"
#include "lightsource.h"
#include "../WaveGenerator/basewavegenerator.h"
#include "../Poligons/poligoncreator.h"

#include <memory>
using namespace std;

class Scene
{
private:
    shared_ptr<BaseWaveGenerator> generator;
    shared_ptr<PoligonCreator> creator;
    Point light;
    Camera camera;
    shared_ptr<HeightMap> hMap;
    shared_ptr<NormalMap> nMap;

    double velocity;
public:
    Scene() : hMap(new HeightMap), nMap(new NormalMap), light(0,1,0){}

    Point &getLight() {return light; }
    Camera& getCamera() {return camera; }
    shared_ptr<Poligons> retScene(double time, double angle);
    void setGenCreat( const shared_ptr<BaseWaveGenerator>& gen, const shared_ptr<PoligonCreator> & creat) {generator = gen; creator = creat;}

    void setVelocity(double v) {velocity = v; hMap->first_time = true; }
    void setNet(int n, int m, double Lx, double Lz) { hMap = shared_ptr<HeightMap>(new HeightMap(n, m, Lx, Lz));}
};

#endif // SCENE_H
