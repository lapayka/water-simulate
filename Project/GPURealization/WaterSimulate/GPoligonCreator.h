#pragma once
#include "Poligons/poligoncreator.h"
#include "DrawManager/basedrawmanager.h"

class GPoligonCreator : public PoligonCreator
{
public:
	GPoligonCreator() = default;
	virtual shared_ptr<Poligons> getPoligons(const shared_ptr<HeightMap> &hMap, const Point &light, double angle) override;
};

