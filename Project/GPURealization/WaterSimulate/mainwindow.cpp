#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>

#include <time.h>

#include "Poligons/cpoligoncreator.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , gscene(new Scene())
{
    ui->setupUi(this);

    //setbuf(stdout, NULL);
    scene = new QGraphicsScene(this);
    ui->Scene->setScene(scene);
    ui->Scene->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    scene->setSceneRect(0, 0, ui->Scene->width(), ui->Scene->height());

    factory = shared_ptr<AbstractFactory>(new CPUFactory);
    curManager = shared_ptr<BaseDrawManager>(new RayTracerManager(scene, shared_ptr<BaseRT>(new ZBuffer)));

    SetGenerator g(factory->createGen(), factory->createPol());

    Facade facade(gscene);
    facade.execute(g);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::generateMBOX(bool ind)
{
    QString mes;
    if (!isParamsSet)
        mes += "Не заданы параметры сетки\n";

    if (!isVelocitySet)
        mes += "Не задана скорость\n";

    if (ind && !isExTimeSet)
        mes += "Не задано время исполнения\n";

    if (!isParamsSet || !isVelocitySet || (ind && !isExTimeSet))
    {
        QMessageBox msgBox;
        msgBox.setText(mes);
        msgBox.exec();
        return true;
    }

    return false;
}



void MainWindow::on_pushButton_clicked()
{
    if (generateMBOX(true))
        return;
    Facade facade(gscene);
    DrawCommand d(time, angle, curManager, timeToExecute * 24);
    time = 0;

    facade.execute(d);
}


void MainWindow::on_V_E_clicked()
{
    double v;
    bool flag;

    v = ui->V_E_2->text().toDouble(&flag);
    if (!flag)
    {
        QMessageBox msgBox;
        msgBox.setText("Введено некоректное значение скорости.");
        msgBox.exec();
        return;
    }
    SetVelocity s_v(v);
    Facade facade(gscene);
    facade.execute(s_v);

    isVelocitySet = true;
}

void MainWindow::on_pushButton_2_clicked()
{
    int n, m;
    double Lx, Lz;
    bool flag1 ,flag2, flag3, flag4;

    n = ui->N_E->text().toInt(&flag1);
    m = ui->NM_E->text().toInt(&flag2);
    Lx = ui->Lx_E->text().toDouble(&flag3);
    Lz = ui->Lz_E->text().toDouble(&flag4);

    if (!flag1 || !flag2 || !flag3 || !flag4 || n <= 0 || m <= 0 || Lx <= 0 || Lz <= 0)
    {
        QMessageBox msgBox;
        msgBox.setText("Введены некорректные параметры.");
        msgBox.exec();
        return;
    }

    SetParams p((n % 2 == 0 ? n : n), (m % 2 == 0 ? m : m), Lx, Lz);
    Facade facade(gscene);
    facade.execute(p);
    isParamsSet = true;
}

void MainWindow::on_pushButton_3_clicked()
{
    double a_angle;
    bool flag;
    a_angle = ui->A_E->text().toDouble(&flag);

    if (!flag)
    {
        QMessageBox msgBox;
        msgBox.setText("Введено некоректное значение угла.");
        msgBox.exec();
        return;
    }

    if (angle + a_angle * M_PI / 180 > M_PI_2 || angle + a_angle * M_PI / 180 < 0)
    {
        QMessageBox msgBox;
        msgBox.setText("Угол должен находиться в пределах от 0 до 90 градусов.");
        msgBox.exec();
        return;
    }

    angle += a_angle * M_PI / 180;
}

void MainWindow::on_pushButton_4_clicked()
{
    bool flag;
    int tmp = ui->Time->text().toInt(&flag);
    if (!flag)
    {
        QMessageBox msgBox;
        msgBox.setText("Введено некоректная продолжительность генерации.");
        msgBox.exec();
        return;
    }

    timeToExecute = tmp;
    isExTimeSet = true;
}

void MainWindow::on_radioButton_clicked()
{
    curManager = shared_ptr<BaseDrawManager>(new CarcasManager(scene));
}

void MainWindow::on_radioButton_2_clicked()
{
    curManager = shared_ptr<BaseDrawManager>(new RayTracerManager(scene, factory->createRT()));
}

void MainWindow::on_pushButton_5_clicked()
{
    if (generateMBOX(false))
        return;
    Facade facade(gscene);
    DrawCommand d(time, angle, curManager, 1);
    time += 0.1;

    facade.execute(d);
}
