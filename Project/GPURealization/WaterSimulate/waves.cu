#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "curand_kernel.h"
#include <iostream>
#include "waves.h"

#define M_PI 3.14
#define EPS 1e-6
#define _A_ 0.5
#define g 9.8

#define K 0.8

struct gComplex
{
	__device__ gComplex() = default;
	__device__ gComplex(double _r, double _i) { r = _r; i = _i; }
	__device__ gComplex(const gComplex& to_copy) { r = to_copy.r; i = to_copy.i; }

	__device__ const gComplex& operator += (const gComplex& com) { r += com.r; i += com.i; return (*this); }
	__device__ gComplex operator *(double num) { gComplex tmp(*this); tmp.r *= num; tmp.i *= num; }
	__device__ const gComplex& operator *= (const gComplex& com) { r = r * com.r - i * com.i; i = r * com.i + com.r * i; return (*this); }
	__device__ gComplex operator * (gComplex rhs) { double tmp = rhs.r;  rhs.r = r * rhs.r - i * rhs.i; rhs.i = r * rhs.i + tmp * i; return rhs; }
	__device__ const gComplex& operator = (const gComplex& to_copy) { r = to_copy.r; i = to_copy.i; return *this; }
	double r;
	double i;
};

__device__ gComplex exp(const gComplex &num)
{
	return gComplex(cos(num.i), sin(num.i));
}

struct gPoint
{
	double x, y, z;
public:
	__device__ gPoint() = default;
	__device__ gPoint(double x, double y)
	{
		this->x = x;
		this->y = y;
		z = 0.0;
	}
	__device__ gPoint(double x, double y, double z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}
	__device__ gPoint operator + (gPoint gPoint) const { gPoint.x += x; gPoint.y += y; gPoint.z += z; return gPoint; }
	__device__ gPoint operator - (gPoint gPoint) const { gPoint.x = x - gPoint.x; gPoint.y = y - gPoint.y; gPoint.z = z - gPoint.z; return gPoint; }
	__device__ gPoint operator * (double num) const { gPoint buf(*this); buf.x *= num; buf.y *= num; buf.z *= num; return buf; }
	__device__ gPoint operator - () const { gPoint buf(-x, -y, -z); return buf; }
	__device__ double operator * (const gPoint &gPoint) const { return x * gPoint.x + y * gPoint.y + z * gPoint.z; }
	const gPoint& operator += (const gPoint& point) { x += point.x; y += point.y; z += point.z; return (*this); }

	__device__ gPoint vector_prod(const gPoint &B) const {
		return gPoint(y * B.z - z * B.y,
			-(x * B.z - z * B.x),
			x * B.y - y * B.x);
	}

	__device__ operator double() const { return sqrt(x * x + y * y + z * z); }
	__device__ double sqr() const { return x * x + y * y + z * z; }
	__device__ gPoint &operator = (const gPoint &_gPoint) { x = _gPoint.x; y = _gPoint.y; z = _gPoint.z; return (*this); }
};

struct Poligon
{
	gPoint normal;
	double D;
	gPoint A, B, C;
	double Ia, Ib, Ic;
	Poligon() = default;

	__device__ Poligon(const gPoint& _A, const gPoint& _B, const gPoint& _C) { A = _A; B = _B; C = _C; }
	__device__ Poligon(const gPoint& _A, const gPoint& _B, const gPoint& _C, const gPoint& n) { A = _A; B = _B; C = _C; normal = n; }
	__device__ Poligon(const gPoint& _A, const gPoint& _B, const gPoint& _C, double ia, double ib, double ic) { A = _A; B = _B; C = _C; Ia = ia; Ib = ib; Ic = ic; }
	__device__ Poligon(const gPoint& _A, const gPoint& _B, const gPoint& _C, const gPoint& n, double _D) { A = _A; B = _B; C = _C; normal = n; D = _D; }

	__device__ void setD(int _D) { D = _D; }
};

__device__ void rotate_scale(gPoint &gPoint, double angle, double k)
{
	double y = gPoint.y;
	gPoint.y = gPoint.y * cos(angle) - gPoint.z * sin(angle);
	gPoint.z = (y * sin(angle) - gPoint.z * cos(angle)) * k;
	gPoint.x = gPoint.x * k;
	gPoint.y = -gPoint.y * k + 800 / 2;
}

__device__ int sign(int c)
{
	if (c < 0)
		return -1;
	else if (c > 0)
		return 1;
	else
		c = 0;

	return c;
}


__device__ gPoint reflect(const gPoint& vec, const gPoint& normal)
{
	return vec - normal * 2.0 * (vec * normal);
}

__device__ double intensity(const gPoint& light, const gPoint& normal)
{
	gPoint reflected(reflect(light, normal));
	double Iref = (reflected * (gPoint(0, 0, -1)));
	Iref *= (1 - K);

	double In = K * (normal * light) + (Iref > 0.0 ? Iref : 0.0) + 0.05;

	return (In < 1.0 ? In : 1.0);
}


void rotate_scale(gPoint& point, double angle, double k)
{
	double y = point.y;
	point.y = point.y * cos(angle) - point.z * sin(angle);
	point.z = (y * sin(angle) - point.z * cos(angle)) * k;
	point.x = point.x * k;
	point.y = -point.y * k + 500;
}

__global__ void fillPointMat(gPoint* pmat, gComplex *h, int n, int m, double Lx, double Lz, double angle, int windWidth)
{
	double x_step = Lx / n;
	double z_step = Lz / m;

	double k = 500 / (Lx - x_step);
	double cur_z = -Lx / 2;

	for (int i = threadIdx.x; i < n; i += blockDim.x)
	{
		for (int j = threadIdx.y; j < m; j += blockDim.y)
		{
			gPoint a(x_step * i, h[i * m + j].r,  z_step * j);
			rotate_scale(a, angle, k);
			pmat[i * m + j] = a;
		}
	}
}

gPoint countNormal(const gPoint& a, const gPoint& b, const gPoint& c, const gPoint& light)
{
	gPoint n((b - a).vector_prod(c - a));
	if ((n * light) < 0)
		n = -n;
	return n;
}

void normalize(gPoint& normal)
{
	normal = normal * (1.0 / double(normal));
}

__global__ void fillNormalMat(gPoint * nmat, gPoint * pmat, int n, int m, const gPoint& light)
{
	for (int i = threadIdx.x; i < n - 1; i += blockDim.x)
		for (int j = threadIdx.y; j < m - 1; j += blockDim.y)
		{
			gPoint n1(countNormal(pmat[i * m + j], pmat[i * m + j + 1],   pmat[(i + 1) * m + j + 1], light));
			gPoint n2(countNormal(pmat[i * m + j], pmat[(i + 1) * m + j], pmat[(i + 1) * m + j + 1], light));

			nmat[(i + 1) * m + j + 1] += n1 + n2;
			cudaThreadSynchronize();
			nmat[i * m + j] += n1 + n2;
			cudaThreadSynchronize();
			nmat[i * m + j + 1] += n1;
			cudaThreadSynchronize();
			nmat[(i + 1) * m + j] += n2;
			cudaThreadSynchronize();
		}

	for (int i = threadIdx.x; i < n; i += blockDim.x)
		for (int j = threadIdx.y; j < m ; j += blockDim.y)
			normalize(nmat[i * m + j]);
}

__global__ void triangulate(Poligon* arr, gPoint * pmat, gPoint* nmat, int n, int m, const gPoint& light)
{
	for (int i = threadIdx.x; i < n - 1; i += blockDim.x)
		for (int j = threadIdx.y; j < m - 1; j += blockDim.y)
		{
			arr[(i * (m - 1) + j) * 2] = Poligon(pmat[i * m + j], pmat[(i + 1) * m + j], pmat[(i + 1) * m + j + 1],
				intensity(light, nmat[i * m + j]), intensity(light, nmat[(i + 1) * m + j]), nmat[(i + 1) * m + j + 1]);
			arr[(i * (m - 1) + j) * 2 + 1] = Poligon(pmat[i * m + j], pmat[i * m + j + 1], pmat[(i + 1) * m + j + 1],
				intensity(light, nmat[i * m + j]), intensity(light, nmat[i * m + j + 1]), nmat[(i + 1) * m + j + 1]);
		}
}




__device__ double Ph(const gPoint &k, double velocity)
{
	double k_2 = k.sqr();
	double L = velocity * velocity / g;

	if (fabs(k_2) < EPS)
		return 0.0;

	return _A_ / (k_2 * k_2) * k.y * k.y * exp(-1.0 / (k_2 * L * L));
}

__device__ gComplex h(const gPoint &k, double velocity)
{
	curandStateSobol64_t state;
	double Ei = curand_normal(&state);
	double Ej = curand_normal(&state);


	return (gComplex(Ei, Ej) * sqrt(Ph(k, velocity) * 0.25 / (sqrt(2.0))));
}

__global__ void cuGenerateSpectr(gComplex *h0, int n, int m, double Lx, double Lz, double velocity)
{
	int n_2 = n / 2;
	int m_2 = m / 2;

	printf("tid %d %d\n", threadIdx.x, threadIdx.y);

	for (int i = threadIdx.x - n_2; i < n_2; i+= blockDim.x)
		for (int j = threadIdx.y - m_2; j < m_2; j+= blockDim.y)
		{
			gPoint k(2.0 * M_PI * i / Lx, 2.0 * M_PI * j / Lz);
			h0[i*n + j] = h(k, velocity);
			printf("(%lf %lf)\n", h0[i*n + j].r, h0[i*n + j].i);
		}
}



__global__ void cuReCountAmplitudes(gComplex *h0, gComplex* h01, int n, int m, double Lx, double Lz, double t)
{
	int n_2 = n / 2;
	int m_2 = m / 2;

	printf("tid %d %d\n", threadIdx.x, threadIdx.y);

	for (int i = threadIdx.x; i < n; i += blockDim.x)
		for (int j = threadIdx.y; j < m; j+= blockDim.y)
		{
			double kx = 2.0 * M_PI * (i - n_2) / Lx;
			double ky = 2.0 * M_PI * (j - m_2) / Lz;
			double w = sqrt(sqrt(kx * kx + ky * ky) * g);

			h0[i * m + j] = h01[i * m + j] * exp(gComplex(0.0, w * t));
		}
}



__global__ void cuGenerateHeightMap(gComplex *h_m, gComplex *h0, int n, int m, double Lx, double Lz)
{
	double mPer_nodex = Lx / n;
	double mPer_nodey = Lz / m;

	int n_2 = n / 2;
	int m_2 = m / 2;

	for (int i = threadIdx.x; i < n; i+= blockDim.x)
		for (int j = threadIdx.y; j < m; j += blockDim.y)
		{
			gComplex h(0,0);
			int a = 1;
			gPoint p(mPer_nodex * (i - n_2), mPer_nodey * (j - m_2));
			for (int ix = 0; ix < n; ix++)
			{
				for (int jy = 0; jy < m; jy++)
				{
					gPoint k(2.0 * M_PI * (ix - n_2) / Lx, 2.0 * M_PI * (jy - m_2) / Lz);
					gComplex tmp = exp(gComplex(0.0, k * p));
					gComplex tmp2 = h0[ix * m + jy];
					if (!isnan(tmp2.r))
					{
						gComplex tmp3 = tmp * tmp2;
						h += tmp3;
					}
					
					
					a += ix;
				}
				h += gComplex(0, 0);
			}
			int c = 2;
			c =c+ 3;
			h_m[i * m + j] = h;
		}
}

void ggenerateSpectr(void *h0, int n, int m, double Lx, double Lz, double velocity)
{
	dim3 grid(16, 16);
	cuGenerateSpectr << <1, grid >> > ((gComplex *)h0, n, m, Lx, Lz, velocity);
}

void greCountAmplitudes(void *h0, void *h01, int n, int m, double Lx, double Lz, double t)
{
	dim3 grid(32, 32);
	cuReCountAmplitudes << <1, grid >> > ((gComplex *)h0, (gComplex *)h1 n, m, Lx, Lz, t);
}

void ggenerateHeightMap(void *h, void *h0, int n, int m, double Lx, double Lz)
{
	dim3 grid(32, 32);
	cuGenerateHeightMap <<<1, grid >>> ((gComplex *)h, (gComplex *)h0, n, m, Lx, Lz);
}


void * cMalloc(int size)
{
	void *ptr;
	cudaMalloc(&ptr, size);

	return ptr;
}

void cFree(void *ptr)
{
	cudaFree(ptr);
}

void CopyFromDevToHost(void *dst, void *src, int size)
{
	cudaMemcpy(dst, src, size, cudaMemcpyDeviceToHost);
}

void CopyFromHostToDev(void *dst, void *src, int size)
{
	cudaMemcpy(dst, src, size, cudaMemcpyHostToDevice);
}

void devSync()
{
	cudaDeviceSynchronize();
}

double ElTime(void* h, void* h0, int n, int m, double Lx, double Lz, double t)
{
	cudaEvent_t start, stop;
	float time;

	dim3 grid(32, 32);
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);

	cuReCountAmplitudes << <1, grid >> > ((gComplex*)h0, n, m, Lx, Lz, t);

	cudaDeviceSynchronize();

	cuGenerateHeightMap << <1, grid >> > ((gComplex*)h, (gComplex*)h0, n, m, Lx, Lz);

	cudaEventRecord(stop, 0);       
	cudaEventSynchronize(stop);     
	cudaEventElapsedTime(&time, start, stop);
	return time;
}


void gCreategPoligons(void* pols, int n, int m, void* h, double Lx, double Lz, double angle, gPoint light)
{
	rotate_scale(light, angle, 1.0);
	light.y = 500 - light.y;


	gPoint* points;
	gPoint* normals;

	cudaMalloc(&points, n * m * sizeof(gPoint));
	cudaMalloc(&normals, n * m * sizeof(gPoint));

	dim3 grid(32, 32);



	fillPointMat << <1, grid >> > (points, h, n, m, Lx, Lz, angle, 500);
	fillNormalMat << <1, grid >> > (normals, points, n, m, light);

	triangulate << <1, grid >> > (pols, points, normals, n, m, light);

	cudaFree(points);
	cudaFree(normals);
}
