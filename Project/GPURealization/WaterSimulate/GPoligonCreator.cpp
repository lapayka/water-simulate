#include "GPoligonCreator.h"

extern "C" void gCreategPoligons(void *pols, int n, int m, void *h, double Lx, double Lz, double angle);

shared_ptr<Poligons> GPoligonCreator::getPoligons(const shared_ptr<HeightMap>& hMap, const Point & light, double angle)
{
	void *mat = (void *)(hMap->gh);

	int n = hMap->n;
	int m = hMap->m;

	shared_ptr<Poligons> pol(new Poligons((n - 1) * (m - 1) * 2));

	pol->setLight(light);

	void* gpols = cMalloc((n-1) * (m-1) * sizeof(Poligon) * 2);

	gCreategPoligons(gpols, n, m, mat, hMap->Lx, hMap->Lz, angle);

	CopyFromDevToHost((void *)pol->array().get(), gpols, pol->size() * sizeof(Poligon));

	cFree(gpols);

	return pol;
}
