#ifndef DRAWCOMMAND_H
#define DRAWCOMMAND_H
#include "basecommand.h"
#include "../DrawManager/basedrawmanager.h"

class DrawCommand : public BaseCommand
{
    double time;
    double *angle;
    int count;
    shared_ptr<BaseDrawManager> drawManager;
public:
    DrawCommand(double _time, double &_angle, const shared_ptr<BaseDrawManager> &dm, int _count){time = _time; drawManager = dm; angle = &_angle; count = _count; }
    void incTime(double _time){time += _time; }
    virtual void excecute(Scene &scene) override;
};

#endif // DRAWCOMMAND_H
