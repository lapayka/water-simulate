#ifndef SETGENERATOR_H
#define SETGENERATOR_H

#include "basecommand.h"
#include "../WaveGenerator/basewavegenerator.h"

class SetGenerator : public BaseCommand
{
    shared_ptr<BaseWaveGenerator> gen;
    shared_ptr<PoligonCreator> creator;
public:
    SetGenerator(const shared_ptr<BaseWaveGenerator> &_gen, const shared_ptr<PoligonCreator>& cre)
        : gen(_gen),
          creator(cre)
    {}
    virtual void excecute(Scene &scene) override {scene.setGenCreat(gen, creator); }
};

#endif // SETGENERATOR_H
