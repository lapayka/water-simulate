#ifndef BASECOMMAND_H
#define BASECOMMAND_H

#include "../Scene/scene.h"

class BaseCommand
{
public:
    BaseCommand() = default;
    virtual void excecute(Scene &scene) = 0;
};

#endif // BASECOMMAND_H
