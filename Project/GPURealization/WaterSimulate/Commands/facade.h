#ifndef FACADE_H
#define FACADE_H

#include "../Scene/scene.h"
#include "basecommand.h"

class Facade
{
    shared_ptr<Scene> scene;
public:
    Facade(const shared_ptr<Scene> &_scene) :
    scene(_scene)
    { };
    void execute(BaseCommand &command) {command.excecute(*scene); }
};

#endif // FACADE_H
