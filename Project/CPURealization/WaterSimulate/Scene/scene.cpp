#include "scene.h"
#include <cfloat>

shared_ptr<Poligons> Scene::retScene(double time, double angle)
{
    generator->generateMaps(hMap, nMap, time, velocity);

    return creator->getPoligons(hMap, light, angle);
}
