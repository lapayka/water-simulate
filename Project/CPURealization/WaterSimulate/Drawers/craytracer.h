#ifndef CRAYTRACER_H
#define CRAYTRACER_H

#include "basert.h"

class CRayTracer : public BaseRT
{
private:
    Color trace(const Poligon *pols, int n, const Ray &ray, const Point &light, double ymax, double ymin);

public:
    CRayTracer() = default;

    virtual void draw(Color *colors, int n, int m, Poligon *pols, int pol_size, const Point &light, double ymax, double ymin)override;

};

#endif // CRAYTRACER_H
