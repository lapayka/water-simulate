#ifndef BASERT_H
#define BASERT_H

#include "../Poligons/poligons.h"
#include "Scene/lightsource.h"
#include <iostream>
#include <QDebug>

struct Color
{
    unsigned char R;
    unsigned char G;
    unsigned char B;

    Color() = default;
    Color(unsigned int A, unsigned int _B, unsigned int C) {R = A; G = _B; B = C; }
    Color(const Color &c) {R = c.R; G = c.G; B = c.B; }


    Color operator + (Color rhs) {rhs.R += R; rhs.G += G; rhs.B += B; return rhs; }
    Color operator * (double num) {Color tmp(*this); tmp.R *= num; tmp.G *= num; tmp.B *= num; return tmp; }
};

ostream & operator << (ostream & cout, const Color &color);


struct Ray
{
    Point vector;
    Point begin;

    Ray(const Point &a, const Point &b)
    {
        begin = a;
        vector = b;
    }

    bool intersect(const Poligon &pol, Point &beg) const;
    Point reflect(const Poligon &pol) const;
};


class BaseRT
{

public:
    BaseRT() = default;

    virtual void draw(Color *colors, int n, int m, Poligon *pols, int pol_size, const Point &light, double ymax, double ymin) = 0;
};

#endif // BASERT_H
