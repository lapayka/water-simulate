#include "basert.h"

#define EPS 1e-6

double square(double a, double b, double c)
{
    double p = (a + b + c) / 2.0;
    return sqrt(p * (p-a) * (p-b) * (p-c));
}

bool Ray::intersect(const Poligon &pol, Point &inter) const
{
    if (fabs(vector * pol.normal) < EPS)
    {
        return false;
    }
    double t = (-((begin * pol.normal + pol.D) / (vector * pol.normal)));
    if (t < EPS)
        return false;

    inter = begin + vector * t;


    double a = double(pol.A - inter);
    double b = double(pol.B - inter);
    double c = double(pol.C - inter);

    double AB = double(pol.B - pol.A);
    double BC = double(pol.C - pol.B);
    double AC = double(pol.C - pol.A);
    if (fabs(square(AB, BC, AC) - square(AB, a, b) - square(BC, c, b) - square(AC, a, c)) < EPS)
        return true;

    return false;
}

Point Ray::reflect(const Poligon &pol) const
{
    return vector - pol.normal * 2.0 * (vector * pol.normal);
}

ostream &operator <<(ostream &cout, const Color &color)
{
    //setbuf(stdout, NULL);
    std::cout << "(" << color.R << " " << color.G << " " << color.B << ")\n";
    return cout;
}
