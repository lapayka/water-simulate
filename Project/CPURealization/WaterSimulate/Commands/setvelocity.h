#ifndef SETVELOCITY_H
#define SETVELOCITY_H

#include "basecommand.h"

class SetVelocity : public BaseCommand
{
    double velocity;
public:
    SetVelocity(double _velocity) {velocity = _velocity; };
    virtual void excecute(Scene &scene) override {scene.setVelocity(velocity); };
};

#endif // SETVELOCITY_H
