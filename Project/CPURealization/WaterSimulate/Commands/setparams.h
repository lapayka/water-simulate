#ifndef SETPARAMS_H
#define SETPARAMS_H

#include "basecommand.h"

class SetParams : public BaseCommand
{
    int n, m;
    double Lx, Lz;
public:
    SetParams(int nDim, int mDim, double _Lx, double _Lz) {n = nDim; m = mDim; Lx = _Lx; Lz = _Lz;};
    virtual void excecute(Scene &scene) override {scene.setNet(n,m,Lx,Lz); };
};

#endif // SETPARAMS_H
