QT       += core gui


QMAKE_CXXFLAGS += -fopenmp

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Poligons/cpoligoncreator.cpp \
    AbstractFactory/abstractfactory.cpp \
    Commands/basecommand.cpp \
    Commands/drawcommand.cpp \
    Commands/facade.cpp \
    Commands/setgenerator.cpp \
    Commands/setparams.cpp \
    Commands/setvelocity.cpp \
    Drawers/basert.cpp \
    Drawers/craytracer.cpp \
    DrawManager/basedrawmanager.cpp \
    DrawManager/carcasmanager.cpp \
    DrawManager/raytracermanager.cpp \
    Maps/heightmap.cpp \
    Maps/normalmap.cpp \
    Matrix/matrix.cpp \
    Poligons/point.cpp \
    Poligons/poligoncreator.cpp \
    Poligons/poligons.cpp \
    Scene/camera.cpp \
    Scene/lightsource.cpp \
    Scene/scene.cpp \
    WaveGenerator/basewavegenerator.cpp \
    WaveGenerator/cwavegenerator.cpp \
    main.cpp \
    mainwindow.cpp \
    Drawers/zbuffer.cpp

HEADERS += \
    Poligons/cpoligoncreator.h \
    AbstractFactory/abstractfactory.h \
    Commands/basecommand.h \
    Commands/drawcommand.h \
    Commands/facade.h \
    Commands/setgenerator.h \
    Commands/setparams.h \
    Commands/setvelocity.h \
    Drawers/basert.h \
    Drawers/craytracer.h \
    DrawManager/basedrawmanager.h \
    DrawManager/carcasmanager.h \
    DrawManager/raytracermanager.h \
    Maps/heightmap.h \
    Maps/normalmap.h \
    Matrix/matrix.h \
    Poligons/point.h \
    Poligons/poligoncreator.h \
    Poligons/poligons.h \
    Scene/camera.h \
    Scene/lightsource.h \
    Scene/scene.h \
    WaveGenerator/basewavegenerator.h \
    WaveGenerator/cwavegenerator.h \
    mainwindow.h \
    Drawers/zbuffer.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
