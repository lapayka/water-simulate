#ifndef POLIGONCREATOR_H
#define POLIGONCREATOR_H

#include <memory>
#include "../Maps/heightmap.h"
#include "../Poligons/poligons.h"

using namespace std;

class PoligonCreator
{
public:
    PoligonCreator() = default;
    virtual ~PoligonCreator() = default;

    virtual shared_ptr<Poligons> getPoligons(const shared_ptr<HeightMap> &hMap, const Point &light,  double angle) =0;

};

#endif // POLIGONCREATOR_H
