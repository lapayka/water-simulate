#ifndef NORMALMAP_H
#define NORMALMAP_H

#include "../Matrix/matrix.h"


struct NormalMap
{
    int n, m;
    NormalMap() = default;
    NormalMap(int _n, int _m) : n(_n), m(_m) {};
    NormalMap(NormalMap &map)
        : n(map.n),
          m(map.m)
          //matrix(map.matrix.release())
    {}
};

#endif // NORMALMAP_H
