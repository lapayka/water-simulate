#ifndef CARCASMANAGER_H
#define CARCASMANAGER_H

#include "basedrawmanager.h"
#include <iostream>
using namespace std;

enum { WINDOW_HEIGHT = 800, WINDOW_WIDTH = 1000 };

class CarcasManager : public BaseDrawManager
{
private:
    void drawPoligon(Poligon &pol, double k, double angle);
    void drawLine(const Point &A, const Point &B) {scene->addLine(A.x, A.y, B.x, B.y); }
public:
    CarcasManager(QGraphicsScene *_scene) {scene = _scene;}
    virtual void draw(Scene &gscene, double time, double angle, int count) override;
};

#endif // CARCASMANAGER_H
