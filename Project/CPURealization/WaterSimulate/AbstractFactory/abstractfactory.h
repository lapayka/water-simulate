#ifndef ABSTRACTFACTORY_H
#define ABSTRACTFACTORY_H

#include <memory>
#include "../WaveGenerator/cwavegenerator.h"
#include "../Poligons/cpoligoncreator.h"
#include "../DrawManager/carcasmanager.h"
#include "../DrawManager/raytracermanager.h"
#include "../Drawers/craytracer.h"
#include "../Drawers/zbuffer.h"

class AbstractFactory
{
public:
    AbstractFactory() = default;

    virtual shared_ptr<BaseWaveGenerator> createGen() = 0;
    virtual shared_ptr<PoligonCreator> createPol() = 0;
    virtual shared_ptr<BaseRT> createRT() = 0;
};

class CPUFactory : public AbstractFactory
{
public:
    CPUFactory() = default;

    virtual shared_ptr<BaseWaveGenerator> createGen() override {return shared_ptr<BaseWaveGenerator>(new CWaveGenerator());}
    virtual shared_ptr<PoligonCreator> createPol() override {return shared_ptr<PoligonCreator>(new CPoligonCreator);}

    virtual shared_ptr<BaseRT> createRT() {return shared_ptr<BaseRT>(new ZBuffer()); }
};

#endif // ABSTRACTFACTORY_H
