#ifndef CWAVEGENERATOR_H
#define CWAVEGENERATOR_H

#include "basewavegenerator.h"
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include "../Poligons/point.h"

class CWaveGenerator : public BaseWaveGenerator
{
public:
    CWaveGenerator() = default;
    virtual void generateMaps(const shared_ptr<HeightMap> &hMap, const shared_ptr<NormalMap> &nMap, double time, double velocity) override;
};
#endif // CWAVEGENERATOR_H
